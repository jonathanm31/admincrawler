<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Clases;

class CrawlerController extends Controller
{
    protected $crawler;
    function __construct() {
        $array = ["nombre" => "h3", "especialidad" => "h4", "datos" => 'div.ps_text_column.datos-doctor > ul > li',"contenedor" => 'div.ps_text_column.doctor-estudio.no-padding'];
        $this->crawler = new Clases\Crawler("https://clinicadelgado.pe/doctores-por-especialidad/",$array);

    }
   public function index(){
        $linkDoctores = '.ps_text_column';
        $this->crawler->index($linkDoctores);
    }
    public function excel(){
        $array = ["nombre" => "h3", "especialidad" => "h4", "datos" => 'div.ps_text_column.datos-doctor > ul > li',"contenedor" => 'div.ps_text_column.doctor-estudio.no-padding'];
        $this->crawler->index($array);
        return $this->crawler->excel();
    }
}