<?php
/**
 * Created by PhpStorm.
 * User: Jorge Bado
 * Date: 24/01/2018
 * Time: 10:27
 */

namespace App\Http\Clases;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class Crawler
{
    protected $cliente;
    protected $doctores;
    protected $selectores;
    protected $headers;
    protected $data;
    protected $url;
    protected $file;
    function __construct($url,$selectores) {
        $this->cliente = new Client();
        $this->url = $url;
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 1200,
        ));
        $this->cliente->setClient($guzzleClient);
        $this->selectores = $selectores;

    }
    public function index($selecterlink){
        $this->getURLs($selecterlink);
        dump($this->data);
    }

    public function getURLs($selecterlink){
        $crawler = $this->cliente->request('GET', $this->url);
        $crawler->filter('div.ps_text_column>ul>li>a')->each(function ($node,$i){
            $this->doctores[$i]=$node->attr('href');
        });

        for($i = 0 ; $i <5  ; $i++){
            try{
                $this->getDatosDoctor($this->doctores[$i]);
            }catch (\Exception $exception){
                echo "salto...<br>";
            }
        }

    }

    protected function getDatosDoctor($url){
        $crawler = $this->cliente->request('GET',$url);
        $nombre = $crawler->filter($this->selectores["nombre"]);
        $especialidad = $crawler->filter($this->selectores["especialidad"]);
        $uls = $crawler->filter($this->selectores['datos']);
        $this->headers = $array_headers = ["nombres","especialidad","cmp","rne"];
        $d = ["nombre" => $nombre->text(), "especialidad" =>$especialidad->text(),"cmp" => filter_var($uls->first()->text(), FILTER_SANITIZE_NUMBER_INT),"rne" =>filter_var($uls->eq(1)->text(), FILTER_SANITIZE_NUMBER_INT)  ];
        $crawler->filter($this->selectores['contenedor'].' ul> li')->each(function ($node, $i) {
            $d []= $node->text();
        });
        $this->data []= $d;
    }
    public function excel(){

       return Excel::create('doctores', function($excel) {
            $excel->sheet('mySheet', function($sheet)
            {
                $sheet->fromArray($this->data);

            });
        })->download();
    }

}